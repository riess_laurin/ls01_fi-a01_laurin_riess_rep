
public class Fahrenheit {

	public static void main(String[] args) {
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");
		System.out.println("-----------------------");
		
		int[] numbs = {-20,-10,0,20,30};
		
		for (int i : numbs) {
			int fahrenheit = i;
			Float degree = fahrenheitToDegree(i);
			
			if (fahrenheit >= 0) {
				System.out.printf("%+-12d|%10.2f\n", fahrenheit , degree);	
			}
			else {
				System.out.printf("%-12d|%10.2f\n", fahrenheit , degree);
			}
			
		}
	}
	
	
	public static Float fahrenheitToDegree(int fahrenheit) {
		Float output;
		
		output = (float) ((fahrenheit - 32) * 5)/9;
		return output;
	}

}
